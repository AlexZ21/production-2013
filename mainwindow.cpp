#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    knowlageBase.loadFromFile("kb.xml");

    qDebug() << knowlageBase.getEntityById(0).name();
    qDebug() << knowlageBase.getRuleById(0).ruleName();

    updateEntitiesList();
    updateRulesList();
    updateOutput();



    //knowlageBase.getProduction();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateRulesList()
{
    ui->rulesList->clear();
    for(QMap< int, Rule >::iterator it = knowlageBase.getRules()->begin(); it != knowlageBase.getRules()->end(); ++it)
    {
        ui->rulesList->addItem((*it).ruleName());
    }
}

void MainWindow::updateEntitiesList()
{
    ui->entitiesList->clear();
    for(QMap< int, Entity >::iterator it = knowlageBase.getEntities()->begin(); it != knowlageBase.getEntities()->end(); ++it)
    {
        ui->entitiesList->addItem((*it).name());
    }
}

void MainWindow::updateOutput()
{

    ui->outputList->clear();
    QStringList *strList = knowlageBase.getProduction();
    for(QStringList::iterator it = strList->begin(); it != strList->end(); ++it)
    {
       QString line = (*it);
       ui->outputList->addItem(*it);
    }
}
