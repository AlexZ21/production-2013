#ifndef KNOWLAGEBASE_H
#define KNOWLAGEBASE_H

#include <QMap>
#include <QStringList>
#include <QDebug>
#include <QFile>
#include <QtXml/QDomDocument>
#include "entity.h"
#include "rule.h"

class KnowlageBase
{
    QMap< int, Entity > entities;
    QMap< int, Rule > rules;
    QStringList productionList;
public:
    KnowlageBase();
    ~KnowlageBase();

    int addEntity(Entity entity);
    int addRule(Rule rule);

    Entity getEntityById(int id);
    Rule getRuleById(int id);

    QMap< int, Entity > *getEntities();
    QMap< int, Rule > *getRules();

    void loadFromFile(QString fileName);
    QStringList *getProduction();

    void checkRules(int currentEntity, int currentRule, int nextEntity);
};

#endif // KNOWLAGEBASE_H
