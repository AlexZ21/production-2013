#include "knowlagebase.h"

KnowlageBase::KnowlageBase()
{
}

KnowlageBase::~KnowlageBase()
{
}

int KnowlageBase::addEntity(Entity entity)
{
    int id = entities.size();
    entities.insert(id, entity);
    return id;
}

int KnowlageBase::addRule(Rule rule)
{
    int id = rules.size();
    rules.insert(id, rule);
    return id;
}

Entity KnowlageBase::getEntityById(int id)
{
    return entities.value(id,Entity());
}

Rule KnowlageBase::getRuleById(int id)
{
    return rules.value(id,Rule());
}

QMap<int, Entity> *KnowlageBase::getEntities()
{
    return &entities;
}

QMap<int, Rule> *KnowlageBase::getRules()
{
    return &rules;
}

void KnowlageBase::loadFromFile(QString fileName)
{
    QDomDocument doc("kb");
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
        return;
    if (!doc.setContent(&file)) {
        file.close();
        return;
    }
    file.close();

    QDomElement docElem = doc.documentElement();
    QDomNode n = docElem.firstChild();

    while(!n.isNull()) {
        QDomElement e = n.toElement();
        if(!e.isNull()) {
            if(e.tagName() == "Entity")
            {
                QDomNode attr = n.firstChild();
                int id = attr.toElement().text().toInt();
                attr = attr.nextSibling();
                QString name = attr.toElement().text();
                entities.insert(id, Entity(name));
            }
            if(e.tagName() == "Rule")
            {
                QDomNode attr = n.firstChild();
                int id = attr.toElement().text().toInt();
                attr = attr.nextSibling();
                int from = attr.toElement().text().toInt();
                attr = attr.nextSibling();
                int to = attr.toElement().text().toInt();
                attr = attr.nextSibling();
                int type = attr.toElement().text().toInt();
                attr = attr.nextSibling();
                QString ruleName = attr.toElement().text();
                Rule newRule(from, to,  (RT::RuleType)type);
                newRule.setRuleName(ruleName);
                rules.insert(id, newRule);
            }
        }
        n = n.nextSibling();
    }

}

QStringList *KnowlageBase::getProduction()
{
    productionList.clear();

    for(QMap< int, Entity >::iterator itEntities = entities.begin(); itEntities != entities.end(); ++itEntities)
    {
        for(QMap< int, Rule >::iterator itRules = rules.begin(); itRules != rules.end(); ++itRules)
        {
            if(itEntities.key() == itRules->getFrom())
            {
                productionList.push_back(entities[itRules->getFrom()].name() + " " + rules[itRules.key()].ruleName() + " " + entities[itRules->getTo()].name());
                qDebug() << entities[itRules->getFrom()].name() + " " + rules[itRules.key()].ruleName() + " " + entities[itRules->getTo()].name();
                checkRules(itRules->getFrom(), itRules.key(), itRules->getTo());
            }
        }
    }

    return &productionList;

}

void KnowlageBase::checkRules(int currentEntity, int currentRule, int nextEntity)
{
    for(QMap< int, Rule >::iterator itRules = rules.begin(); itRules != rules.end(); ++itRules)
    {
        if(nextEntity == itRules->getFrom())
        {

            switch (rules[currentRule].getRuleType()) {

            case RT::Isa:

                switch (itRules->getRuleType()) {
                case RT::Isa:
                    productionList.push_back(entities[currentEntity].name() + " " + rules[currentRule].ruleName() + " " + entities[itRules->getTo()].name());
                    checkRules(currentEntity, itRules.key(), itRules->getTo());
                    break;
                case RT::DoesTo:
                    productionList.push_back(entities[currentEntity].name() + " " + itRules->ruleName() + " " + entities[itRules->getTo()].name());
                    checkRules(currentEntity, itRules.key(), itRules->getTo());
                    break;
                default:
                    break;
                }
                break;

            case RT::PartOf:

                switch (itRules->getRuleType()) {
                case RT::PartOf:
                    productionList.push_back(entities[currentEntity].name() + " " + itRules->ruleName() + " " + entities[itRules->getTo()].name());
                    checkRules(currentEntity, itRules.key(), itRules->getTo());
                    break;
                default:
                    break;
                }
                break;

            case RT::BelongTo:

                switch (itRules->getRuleType()) {
                case RT::BelongTo:
                    productionList.push_back(entities[currentEntity].name() + " " + itRules->ruleName() + " " + entities[itRules->getTo()].name());
                    checkRules(currentEntity, itRules.key(), itRules->getTo());
                    break;
                default:
                    break;
                }
                break;

            case RT::DoesTo:

                switch (itRules->getRuleType()) {
                case RT::AddAttr:
                    productionList.push_back(entities[currentEntity].name() + " " + itRules->ruleName() + " " + entities[itRules->getTo()].name());
                    checkRules(currentEntity, itRules.key(), itRules->getTo());
                    break;
                case RT::Isa:
                    productionList.push_back(entities[currentEntity].name() + " " + rules[currentRule].ruleName() + " " + entities[itRules->getTo()].name());
                    checkRules(currentEntity, itRules.key(), itRules->getTo());
                    break;
                default:
                    break;
                }
                break;

            case RT::Contains:

                switch (itRules->getRuleType()) {
                case RT::Isa:
                    productionList.push_back(entities[currentEntity].name() + " " + rules[currentRule].ruleName() + " " + entities[itRules->getTo()].name());
                    checkRules(currentEntity, itRules.key(), itRules->getTo());
                    break;
                case RT::DoesTo:
                    productionList.push_back(entities[currentEntity].name() + " " + itRules->ruleName() + " " + entities[itRules->getTo()].name());
                    checkRules(currentEntity, itRules.key(), itRules->getTo());
                    break;
                default:
                    break;
                }
                break;

            default:
                break;
            }
        }
    }
}


