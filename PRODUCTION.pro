#-------------------------------------------------
#
# Project created by QtCreator 2013-12-19T21:10:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PRODUCTION
TEMPLATE = app

QT += xml


SOURCES += main.cpp\
        mainwindow.cpp \
    knowlagebase.cpp \
    rule.cpp

HEADERS  += mainwindow.h \
    entity.h \
    knowlagebase.h \
    rule.h

FORMS    += mainwindow.ui
