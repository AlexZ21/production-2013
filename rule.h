#ifndef RULE_H
#define TRULE_H

#include <QString>

class RT {
public:
    enum RuleType{
        Isa,
        BelongTo,
        PartOf,
        Contains,
        DoesTo,
        AddAttr
    };

    static const char* defaultRulesName[];
};


class Rule
{
    int from, to;
    RT::RuleType ruleType;
    QString ruleTypeName;
public:  
    Rule()
    {
        this->from = 0;
        this->to = 0;
        ruleType = RT::Isa;
        ruleTypeName = RT::defaultRulesName[RT::Isa];
    }

    Rule(int from, int to, RT::RuleType type)
    {
        this->from = from;
        this->to = to;
        ruleType = type;
        ruleTypeName = RT::defaultRulesName[type];
    }

    void set(int from, int to)
    {
        this->from = from;
        this->to = to;
    }

    void setType(RT::RuleType type)
    {
        ruleType = type;
    }

    void setRuleName(QString name)
    {
        ruleTypeName = name;
    }

    QString ruleName()
    {
        return ruleTypeName;
    }

    int getRuleType()
    {
        return ruleType;
    }

    int getFrom()
    {
       return from;
    }

    int getTo()
    {
        return to;
    }
};

#endif // RULE_H
