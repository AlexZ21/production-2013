#ifndef ENTITY_H
#define ENTITY_H

#include <QString>

class Entity
{
    QString entityName;
public:
    Entity(){this->entityName = "";}
    Entity(QString entityName){this->entityName = entityName;}
    QString name()
    {
        return entityName;
    }
};

#endif // ENTITY_H
